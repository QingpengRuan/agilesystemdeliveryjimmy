import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import junit.framework.TestCase;

public class DatabaseTest extends TestCase 
{
/*		//Test No. 1
		//Objective: To test entry of customer data
		//Input(s): First Name = "Kevin", Surname = "McQuaide", Address = "Longford", Phone Number = "0851181958"
		//Expected Output: True
	public void testCustomerEntry001() throws SQLException {
		Database testObject = new Database();
		assertEquals(true, testObject.addNewCustomer("Kevin", "McQuaide", "Longford", "0851181958"));// Will check the expected output "true" matches the
																									// output from the method
	}

	//Test No. 2
	//Objective: To test entry of delivery person data
	//Input(s): First Name = "Walter", Surname = "White", Area = "Athone" 
	//Expected Output: True
	public void testDeliveryPersonEntry001() {
		Database testObject = new Database();
		assertEquals(true, testObject.addNewDelivery("Walter", "White", "Athlone"));		// Will check the expected output "true" matches the
																									// output from the method
	}
	
	//Test No. 3
	//Objective: To test entry of subscription data
	//Input(s): Holiday Start = "13/07/2018", Holiday End = "20/07/2018" 
	//Expected Output: True
	public void testSubscriptionEntry001() throws SQLException, ParseException {
		Database testObject = new Database();
		assertEquals(true, testObject.addNewSubscription("13/07/2018", "20/07/2018", false, false, false, false));		// Will check the expected output "true" matches the
																									// output from the method
	}
	
	//Test No. 4
	//Objective: To test entry of docket
	//Input(s): Area = "Dublin", Delivery person first name = "Jesse", Delivery person surname = "Pinkman", Customer first name "Saul", Cust surname "Goodman", sub id 1
	//Expected Output: True
	public void testDocketEntry001() throws SQLException, ParseException {
		Database testObject = new Database();
		assertEquals(true, testObject.addNewDocket("Dublin", "Jesse", "Pinkman", "Saul", "Goodman", 1));		// Will check the expected output "true" matches the
		// output from the method
	}
	//Test No. 5
	//Objective: To test searching non-existant customer by an ID returns null
	//Input(s): Cust ID: 47
	//Expected Output: null
	public void testCustSearchId001() throws SQLException, ParseException {
		Database testObject = new Database();
		assertEquals(null, testObject.searchCustomerID(47));		// Will check the expected output null matches the empty result set as the id shouldn't be found
		// output from the method
	}
	
	
	//Test No. 6
	//Objective: To test searching non-existant customer by an first and surname returns null
	//Input(s): First name: "Jill", Surname: "Austin"
	//Expected Output: null
	public void testCustSearchName001() throws SQLException, ParseException {
		Database testObject = new Database();
		assertEquals(null, testObject.searchCustomerName("Jill", "Austin"));		// Will check the expected output null matches the empty result set as the id shouldn't be found
		// output from the method
	}
	
	//Test No. 7
	//Objective: To test updating of customer by changing their details
	//Input(s): Cust ID: 4 First name: "James", Surname: "Mangold", Address: "Athlone", Phone number: "0879891234"
	//Expected Output: true
	public void testCustModify001() throws SQLException, ParseException {
		Database testObject = new Database();
		assertEquals(true, testObject.modifyCustomer(4, "James", "Mangold", "Athlone", "0879891234"));		// Will check the expected output is true as it should update that customer
		// output from the method
	}
	

	//Test No. 8
	//Objective: To test updating of customer won't work if ID doesn't exist 
	//Input(s): Cust ID: 47 First name: "James", Surname: "Mangold", Address: "Athlone", Phone number: "0879891234"
	//Expected Output: true
	public void testCustModify002() throws SQLException, ParseException {
		Database testObject = new Database();
		assertEquals(false, testObject.modifyCustomer(47, "James", "Mangold", "Athlone", "0879891234"));		// Will check the expected output is false as it should not find the customer
		// output from the method
	}*/

	//Test No. 9
	//Objective: To test searching of subscription works if ID does exist 
	//Input(s): Sub ID: 9
	//Expected Output: true
	public void testSubSearchSubID001() throws SQLException, ParseException {
		Database testObject = new Database();
		
		ResultSet rs = testObject.searchSubscriptionID(9);
		
		assertNotNull(rs);	
	}


	//Test No. 10
	//Objective: To test searching of subscription works if ID does not exist 
	//Input(s): Sub ID: 100
	//Expected Output: false
	public void testSubSearchSubID002() throws SQLException, ParseException {
		Database testObject = new Database();
		
		ResultSet rs = testObject.searchSubscriptionID(100);
		
		assertNull(rs);	
	}
	
}